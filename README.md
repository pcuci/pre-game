# game

The purpose of "the game" is to surface collaboration opportunities effectively and efficiently for all participating agents.

Play to win-win-win.

- Win for you.
- Win for us.
- Win for the environment, i.e.: everything not playing the game.

## Table of Contents

- [Who We Are](#who-we-are)
- [Interaction Protocol or API](#interaction-protocol-or-API)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License & Attribution](#license-attribution)

## Who We Are

Long-term minded world-wide innovation-driven entrepreneurial contributors.

We are the operators and designers of the Sustainable Social Operating System, the SSOS for short, aka "the game". We play an infinite set.

We believe we can benefit each other's existence more by being radically transparent and explicit about how we decide to work together to achieve an unprecedented number of win-win-win outcomes.

## Why a SSOS?

The human systems we put in place around the globe need better coordination and resource management workflows that benefit even more people in the long run. We believe we can do better than the status quo.

In technology, an operating system (OS) is system software that manages computer hardware and software resources and provides common services for computer programs, which in turn are used clients (other software or human).

We also have people and their action-potential bodies and minds, their hardware and 'wetware', respectively. The core OS ideas can be expended to human agents interacting with hardware and software under a predetermined set of engagemnt rules we call "the game". This would make the OS social, or an SOS.

To further refine our operating system and ensure it mutually benefits all players involved, we need the SOS to also be sustainable, a SSOS.

> **SSOS** is a Sustainable Social Operating System for biological and software agents

Furthermore, planetary activities can be organized around the following 4 time scales the SSOS must handle.

**Event dynamics (minutes, hours)**
moderators, presenters, speaking, turn taking, broad feedback
Projects, Ongoing Activities, Medium-term Value 

**Creation (days to years)**
ongoing group activities focused on a value creation objective

**Lifelong Wellbeing (a lifetime)**
growth, learning, contribution, value creation, support, reward

**Generational (families, organizations, governments, ations)**
long term activities, investments (energy, resources, time, effort), succession, value creation, sustaining support/rewards

## Interaction Protocol or API

We rely on feedback as the core value-building block of the SSOS.

Today's segment-by-segment protocol consists of 3 stages:

- **the hashtag #** - what is it that just happened
- **the heart <3** - what is there to appreciate about what just happened
- **the delta Δ** - what to consider improving

This protocol is a living system, evolving, not enough for successful collaboration. The rest of the game shall highlight additional meta-constraints of play.

## Install

The minimal requirement is another willing agent: 2+ people or systems need to interact.

## Usage

Spot an opportunity (need/want) and share it with a set of observers to gain in-the-moment and long-term feedback.

Engage the observeres to feedback in #-<3-Δ format.

Implement in your team and organization. The game is scale-free, and so the mechanics work the same between individuals as between organizations and nation-states.

## Maintainers

- Paul Cuciureanu [@pcuci](https://gitlab.com/pcuci)
- Leo Hartman [@leohartman](https://gitlab.com/leohartman)
- David Rowley [@david-ro](https://gitlab.com/david-ro)

The maintainers' job is to orchestrate community consensus around the rules of the game. `TODO: How to become a maintainer?`

## Contributing

***You decide your level of involvement!*** *—Tyler Durden, Fight Club*

Feel free to dive in! [Open an issue](https://gitlab.com/samergo/agents/game/issues/new) or submit PRs.

SamErgo follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.

> "sam ergo", meaning "us motion", is the current SSOS code name, much like how Linux, Windows and Mac are operating system names too. 💻🌏

## License & Attribution

The SamErgo SSOS is available under the [Creative Commons CC0 1.0 License](https://creativecommons.org/publicdomain/zero/1.0/), meaning you are free to use it for any purpose, commercial or non-commercial, without any attribution back to the original authors (public domain).